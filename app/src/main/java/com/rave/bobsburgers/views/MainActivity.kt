package com.rave.bobsburgers.views

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.rave.bobsburgers.model.remote.RetrofitClass
import com.rave.bobsburgers.model.CharacterRepo
import com.rave.bobsburgers.model.mapper.CharacterMapper
import com.rave.bobsburgers.ui.theme.BobsBurgersTheme
import com.rave.bobsburgers.viewmodel.CharacterViewModel
import com.rave.bobsburgers.viewmodel.VMlFactory
import com.rave.bobsburgers.views.characterscreen.CharacterScreen
import com.rave.bobsburgers.views.characterscreen.CharacterScreenState

class MainActivity : ComponentActivity() {

    private val characterViewModel by viewModels<CharacterViewModel> {
        val fetcher = RetrofitClass.getCharacterFetcher()
        val repo = CharacterRepo(fetcher, CharacterMapper())
        VMlFactory(repo)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        characterViewModel.getCharacters()
        setContent {
            val characterState by characterViewModel.characterState.collectAsState()
            BobsBurgersTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CharacterListApp(characterState)
                }
            }
        }
    }
}

@Composable
fun CharacterListApp(characters: CharacterScreenState){
    CharacterScreen(characters)
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    BobsBurgersTheme {
        Greeting("Android")
    }
}