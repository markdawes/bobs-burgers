package com.rave.bobsburgers.model.remote.dtos

import kotlinx.serialization.Serializable

// @Serializable
class CharacterResponse : ArrayList<CharacterDTO>()